﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using NewAPIChurchAPP.Models;

namespace NewAPIChurchAPP
{
    public partial class resetpassword : System.Web.UI.Page
    {
        Utility.CustomResponse _res = new Utility.CustomResponse();
        protected void Page_Load(object sender, EventArgs e)
        {

            lblmsg.Visible = false;
            txtPassword.Visible = true;
            btnSubmit.Visible = true;
            lblpwd.Visible = true;
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {

            // string header = Request.Headers.GetValues("AppID").ToString();
            string baseUrl = ConfigurationManager.AppSettings["BaseURL"];
            string token = Request.QueryString["token"];
            string username = Request.QueryString["username"];
            string password = txtPassword.Text.Trim();

            HttpClient client = new HttpClient { BaseAddress = new Uri(baseUrl) };
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            var response = client.GetAsync("Users/UpdatePassword?userName=" + username + "&password=" + password).Result;
            if (response.IsSuccessStatusCode)
            {
                _res = response.Content.ReadAsAsync<Utility.CustomResponse>().Result;
                if (_res.Status == Utility.CustomResponseStatus.Successful)
                {

                    lblmsg.Visible = true;
                    lblmsg.Text = _res.Message;
                    lblmsg.ForeColor = System.Drawing.Color.Black;
                    txtPassword.Visible = false;
                    btnSubmit.Visible = false;
                    lblpwd.Visible = false;
                    //Response.Redirect("http://lemonaid.cc/app-confirmation.html", true);
                }
                else
                {
                    lblmsg.ForeColor = System.Drawing.Color.Red;
                    lblmsg.Visible = true;
                    lblmsg.Text = _res.Message;
                }

            }


        }
    }
}