﻿using NewAPIChurchAPP.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace NewAPIChurchAPP.Controllers
{
    public class Sermons1Controller : ApiController
    {
        public static string Message = "";
        Utility.CustomResponse Result = new Utility.CustomResponse();

        [HttpGet]
        public Utility.CustomResponse  Sermons(int Churchid)
        {
            try
            {

                List<SermonDTO> sermons = ChurchDAL.GetAllSermons(Churchid);
                if (sermons.Count > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = sermons;
                    Result.Message = CustomConstants.DetailsGetSuccessfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.NoRecordsFound;
                }
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }


        [HttpGet]
        public Utility.CustomResponse GetSermon(long sermonId)
        {
            Utility.CustomResponse res = new Utility.CustomResponse();
            try
            {
                var sermon = ChurchDAL.GetSermon(sermonId);
                if (sermon.Id > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = sermon;
                    Result.Message = CustomConstants.DetailsGetSuccessfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.NoRecordsFound;
                }
                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;
            }
        }


        [HttpPost]
        public Utility.CustomResponse CreateSermon(SermonDTO sermonDTO)
        {
            try
            {
                int sermonId = ChurchDAL.CreateSermons(sermonDTO);

                if (sermonId > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = sermonId;
                    Result.Message = CustomConstants.SermonAddedSuccessfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.AccessDenied;
                }

                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;
            }
        }


        [HttpPut]
        public Utility.CustomResponse EditSermon(SermonDTO sermonDTO)
        {
            try
            {
                int sermonId = ChurchDAL.EditSermon(sermonDTO);

                if (sermonId > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = sermonId;
                    Result.Message = CustomConstants.SermonUpdatedSuccessfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.AccessDenied;
                }

                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;
            }
        }


        [HttpDelete]
        public Utility.CustomResponse DeleteSermon(int sermonId)
        {
            try
            {
                int SermonId = ChurchDAL.DeleteSermon(sermonId);

                if (SermonId > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = SermonId;
                    Result.Message = CustomConstants.SermonDeletedSuccessfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.AccessDenied;
                }

                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;
            }
        }
    }
}
