﻿using Microsoft.WindowsAzure.Storage;
using NewAPIChurchAPP.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace NewAPIChurchAPP.Controllers
{
    public class ImagesController : ApiController
    {
        public static string Message = "";
        Utility.CustomResponse Result = new Utility.CustomResponse();

        [HttpGet]
        public Utility.CustomResponse Images(int Churchid)
        {
            try
            {

                List<ImageDTO> Images = ChurchDAL.GetAllImages(Churchid);
                if(Images.Count > 0)
                { 
                Result.Status = Utility.CustomResponseStatus.Successful;
                Result.Response = Images;
                Result.Message = CustomConstants.DetailsGetSuccessfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.NoRecordsFound;
                }
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }


        [HttpGet]
        public Utility.CustomResponse GetImage(int ImageId)
        {
            Utility.CustomResponse res = new Utility.CustomResponse();
            try
            {
                var Image = ChurchDAL.GetImage(ImageId);
                if (Image.Id > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = Image;
                    Result.Message = CustomConstants.DetailsGetSuccessfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.NoRecordsFound;
                }

                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;
            }
        }

        [HttpPost]
        public Utility.CustomResponse UploadToImageGallery(UploadImageDTO objimage)
        {
            try
            {
                Random r = new Random();

                string filename = r.Next(9999).ToString() + DateTime.Now.Millisecond + r.Next(9999) + r.Next(9999).ToString() + DateTime.Now.Millisecond;

                // Retrieve storage account from connection string.
                CloudStorageAccount storageAccount = CloudStorageAccount.Parse(ConfigurationManager.AppSettings["StorageConnectionString"].ToString());
                // Create the blob client.
                Microsoft.WindowsAzure.Storage.Blob.CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();

                // Retrieve a reference to a container. 
                Microsoft.WindowsAzure.Storage.Blob.CloudBlobContainer container = blobClient.GetContainerReference("mycontainer");

                // Create the container if it doesn't already exist.
                container.CreateIfNotExists();


                container.SetPermissions(new Microsoft.WindowsAzure.Storage.Blob.BlobContainerPermissions
                {
                    PublicAccess =
                        Microsoft.WindowsAzure.Storage.Blob.BlobContainerPublicAccessType.Blob
                });


                // Retrieve reference to a blob named "myblob".
                Microsoft.WindowsAzure.Storage.Blob.CloudBlockBlob blockBlob = container.GetBlockBlobReference(filename);

                // Create or overwrite the "myblob" blob with contents from a local file.

                byte[] binarydata = Convert.FromBase64String(objimage.binary);

                //using (var fileStream = binarydata)
                //{
                blockBlob.Properties.ContentType = objimage.format;
                blockBlob.UploadFromByteArray(binarydata, 0, binarydata.Length);

                //UploadFromStream(fileStream);
                //}


                // Retrieve reference to a blob named "myblob".
                Microsoft.WindowsAzure.Storage.Blob.CloudBlockBlob blockBlob1 = container.GetBlockBlobReference(filename);

                string imageurl = blockBlob1.Uri.ToString();
                // -- todo: rohan - save in db so we can clean up later.
                // UsersDAL.StoreImageInDB(imageurl, 1);
                objimage.binary = imageurl;
                UploadImageDTO uploadImageDTO = new UploadImageDTO();
                uploadImageDTO.Id = ChurchDAL.UploadToImageGallery(objimage);
                uploadImageDTO.binary = imageurl;
                if (uploadImageDTO.Id == -99)
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.ImagesMaximumNumberExceeds;
                }
                else if (uploadImageDTO.Id > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Message = CustomConstants.ImageUploadSuccessfully;
                    Result.Response = uploadImageDTO;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.NoRecordsFound;
                }

                return Result;
            }
            catch (Exception ex)
            {
              
                Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                Result.Message = "Failed to add image";
                return Result;
            }

          
        }

        [HttpPut]
        public Utility.CustomResponse UpdateToImageGallery(UploadImageDTO objimage)
        {
            try
            {
                Random r = new Random();

                string filename = r.Next(9999).ToString() + DateTime.Now.Millisecond + r.Next(9999) + r.Next(9999).ToString() + DateTime.Now.Millisecond;

                // Retrieve storage account from connection string.
                CloudStorageAccount storageAccount = CloudStorageAccount.Parse(ConfigurationManager.AppSettings["StorageConnectionString"].ToString());
                // Create the blob client.
                Microsoft.WindowsAzure.Storage.Blob.CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();

                // Retrieve a reference to a container. 
                Microsoft.WindowsAzure.Storage.Blob.CloudBlobContainer container = blobClient.GetContainerReference("mycontainer");

                // Create the container if it doesn't already exist.
                container.CreateIfNotExists();


                container.SetPermissions(new Microsoft.WindowsAzure.Storage.Blob.BlobContainerPermissions
                {
                    PublicAccess =
                        Microsoft.WindowsAzure.Storage.Blob.BlobContainerPublicAccessType.Blob
                });


                // Retrieve reference to a blob named "myblob".
                Microsoft.WindowsAzure.Storage.Blob.CloudBlockBlob blockBlob = container.GetBlockBlobReference(filename);

                // Create or overwrite the "myblob" blob with contents from a local file.

                byte[] binarydata = Convert.FromBase64String(objimage.binary);

                //using (var fileStream = binarydata)
                //{
                blockBlob.Properties.ContentType = objimage.format;
                blockBlob.UploadFromByteArray(binarydata, 0, binarydata.Length);

                //UploadFromStream(fileStream);
                //}


                // Retrieve reference to a blob named "myblob".
                Microsoft.WindowsAzure.Storage.Blob.CloudBlockBlob blockBlob1 = container.GetBlockBlobReference(filename);

                string imageurl = blockBlob1.Uri.ToString();
                // -- todo: rohan - save in db so we can clean up later.
                // UsersDAL.StoreImageInDB(imageurl, 1);
                objimage.binary = imageurl;

               if(ChurchDAL.UpdateToImageGallery(objimage) > 0)
               { 
                Result.Status = Utility.CustomResponseStatus.Successful;
                Result.Message = CustomConstants.ImageUpdateSuccessfully;
                Result.Response = imageurl;
               }
               else
               {
                   Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                   Result.Message = CustomConstants.NoRecordsFound;
               }
                return Result;
            }
            catch (Exception ex)
            {
                //lblstatus.Text = "Failed to upload image to Azure Server";
                Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                Result.Message = "Failed to add image";
                return Result;
            }

          
        }


        [HttpDelete]
        public Utility.CustomResponse DeleteImageGallery(int ImageId)
        {
            try
            {
                int imageId = ChurchDAL.DeleteImageGallery(ImageId);

                if (imageId > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = imageId;
                    Result.Message = CustomConstants.ImageDeleteSuccessfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.AccessDenied;
                }

                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;
            }
        }
    }
}
