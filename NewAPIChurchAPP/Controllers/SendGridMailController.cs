﻿using NewAPIChurchAPP.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using NewAPIChurchAPP.Models;

namespace Lemonaid_web_api.Models
{
    public class SendGridMailController : ApiController
    {
        readonly Utility.CustomResponse _result = new Utility.CustomResponse();

        [HttpGet]
        public Utility.CustomResponse SendGridSendMail()
        {
            //API KEY: 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
            try
            {
                SendGridMailModel sendGridModel = new SendGridMailModel();
                sendGridModel.APIKey = "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";
                sendGridModel.toMail = new List<string>();
                sendGridModel.toMail.Add("mrkiran31@gmail.com");
                sendGridModel.fromMail = "support@yahoo.com";
                sendGridModel.fromName = "you tube team";
                sendGridModel.subject = "you tube video uploading team";
                sendGridModel.Message = "Your video is uploaded successfully and it is available to the viewers after 2 hours" + "https://youtu.be/t9ujp6wJBeE";
                int result = SendGridSMTPMail.SendEmail(sendGridModel);
                _result.Message = "";
                _result.Response = result.ToString();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return _result;
        }
    }
}
