﻿using NewAPIChurchAPP.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;

namespace NewAPIChurchAPP.Controllers
{
    public class CustomWordController : ApiController
    {
        //
        // GET: /CustomWord/

        public static string Message = "";
        Utility.CustomResponse Result = new Utility.CustomResponse();

        [System.Web.Http.HttpGet]
        public Utility.CustomResponse GetCustomWords(int churchId)
        {
            try
            {

                List<CustomWordOfDay> notes = ChurchDAL.GetCustomWordsByChurchId(churchId);
                if (notes.Count > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = notes;
                    Result.Message = CustomConstants.DetailsGetSuccessfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.NoRecordsFound;
                }
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }

        [System.Web.Http.HttpPost]
        public Utility.CustomResponse CreateCustomWords(CustomWordOfDay customWordsDto)
        {
            try
            {

                var words = ChurchDAL.CreateCustomWords(customWordsDto);

                if (words > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = words;
                    Result.Message = CustomConstants.CustomWordsAddedSuccessfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.AccessDenied;
                }

                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;
            }
        }

        [System.Web.Http.HttpPost]
        public Utility.CustomResponse UpdateCustomWords(CustomWordOfDay customWordsDto)
        {
            try
            {

                var words = ChurchDAL.UpdateCustomWords(customWordsDto);

                if (words > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = words;
                    Result.Message = CustomConstants.CustomWordsUpdatedSuccessfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.AccessDenied;
                }

                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;
            }
        }

        [System.Web.Http.HttpGet]
        public Utility.CustomResponse GetCustomWordsByDay(int churchId)
        {
            try
            {

                var notes = ChurchDAL.GetCustomWordsByDay(churchId);
                if (notes != null)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = notes;
                    Result.Message = CustomConstants.DetailsGetSuccessfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.NoRecordsFound;
                }
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }

        [System.Web.Http.HttpGet]
        public Utility.CustomResponse GetCustomWordOfDay(int churchId, int typeId)
        {
            try
            {
                CustomWordsDto verse = null;
                if (typeId == 1)
                {
                     verse = ChurchDAL.GetGenericWordOftheDay();
                }
                else if (typeId == 2)
                {
                     verse = ChurchDAL.GetCustomWordOftheDay(churchId);
                    if (verse == null)
                    {
                        verse = ChurchDAL.GetGenericWordOftheDay();
                    }
                }
                else if(typeId == 3)
                {
                    verse = ChurchDAL.GetNotesOftheDay(churchId);
                    if (verse == null || verse.Verse == null)
                    {
                        verse = ChurchDAL.GetCustomWordOftheDay(churchId);
                        if (verse == null)
                        {
                            verse = ChurchDAL.GetGenericWordOftheDay();
                        }
                    }
                }
                if (verse != null)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = verse;
                    Result.Message = CustomConstants.DetailsGetSuccessfully;  
                }
               else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.NoRecordsFound;
                }
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }
  
    }
}
