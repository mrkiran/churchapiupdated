﻿using NewAPIChurchAPP.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace NewAPIChurchAPP.Controllers
{
    public class TestimonialsController : ApiController
    {
        public static string Message = "";
        Utility.CustomResponse Result = new Utility.CustomResponse();

        [HttpGet]
        public Utility.CustomResponse  Testimonials(int ChurchId)
        {
            try
            {

                List<TestimonialsDTO> testimonials = ChurchDAL.GetAllTestimonials(ChurchId);
                if (testimonials.Count > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = testimonials;
                    Result.Message = CustomConstants.DetailsGetSuccessfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.NoRecordsFound;
                }
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }

        [HttpGet]
        public Utility.CustomResponse AllTestimonials(int ChurchId)
        {
            try
            {

                List<TestimonialsDTO> testimonials = ChurchDAL.GetTotalTestimonials(ChurchId);
                if (testimonials.Count > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = testimonials;
                    Result.Message = CustomConstants.DetailsGetSuccessfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.NoRecordsFound;
                }
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }


        [HttpGet]
        public Utility.CustomResponse GetTestimonial(int testimonialId)
        {
            Utility.CustomResponse res = new Utility.CustomResponse();
            try
            {
                var events = ChurchDAL.GetTestimonial(testimonialId);
                if (events.Id > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = events;
                    Result.Message = CustomConstants.DetailsGetSuccessfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.NoRecordsFound;
                }
                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;
            }
        }

        [HttpPost]
        public Utility.CustomResponse CreateTestimonial(TestimonialsDTO testimonialDTO)
        {
            try
            {
                int testimonialId = ChurchDAL.CreateTestimonial(testimonialDTO);

                if (testimonialId > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = testimonialId;
                    Result.Message = CustomConstants.TestimonialAddedSuccessfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.AccessDenied;
                }
               
                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result; 
            }
        }

        [HttpPost]
        public Utility.CustomResponse EditTestimonial(TestimonialsDTO testimonialDTO)
        {
            try
            {
                int testimonialId = ChurchDAL.EditTestimonial(testimonialDTO);

                if (testimonialId > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = testimonialId;
                    Result.Message = CustomConstants.TestimonialUpdatedSuccessfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.AccessDenied;
                }

                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;
            }
        }


        [HttpPost]
        public Utility.CustomResponse ApproveTestimonial(int testimonialId, int status)
        {
            try
            {
                int id = ChurchDAL.ApproveTestimonial(testimonialId, status);

                if (id > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = testimonialId;
                    if (status == 1)
                    {
                        Result.Message = CustomConstants.TestimonialApprovedSuccessfully;
                    }
                    else if(status == 2)
                    {

                        Result.Message = CustomConstants.TestimonialRejectedSuccessfully;
                    }
                    else if (status == 3)
                    {

                        Result.Message = CustomConstants.TestimonialPublishedSuccessfully;
                    }
                    else if (status == 5)
                    {

                        Result.Message = CustomConstants.TestimonialDeletedSuccessfully;
                    }
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.AccessDenied;
                }

                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;
            }
        }

        [HttpGet]
        public Utility.CustomResponse GetTestimonialByStatus(int churchid,int status)
        {
            Utility.CustomResponse res = new Utility.CustomResponse();
            try
            {
                var Testimonials = ChurchDAL.GetTestimonialByStatus(churchid,status);
                if (Testimonials.Count > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = Testimonials;
                    Result.Message = CustomConstants.DetailsGetSuccessfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.NoRecordsFound;
                }
                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;
            }
        }

      
    }
}
